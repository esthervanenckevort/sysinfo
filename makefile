.SUFFIXES: .c .obj 

.all: sysinfo.exe

.c.obj:
    @echo " Compile::C++ Compiler "
    icc.exe /Ss /Ti /C %s

sysinfo.exe: sysinfo.obj sysinfo.def
    @echo " Link::Linker "
    icc.exe @<<
     /B" /de"
     /Fesysinfo.exe 
     os2386.lib 
     sysinfo.def
     sysinfo.obj
<<

sysinfo.obj: sysinfo.c
